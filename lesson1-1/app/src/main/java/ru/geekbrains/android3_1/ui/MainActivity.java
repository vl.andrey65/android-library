package ru.geekbrains.android3_1.ui;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import ru.geekbrains.android3_1.R;
import ru.geekbrains.android3_1.mvp.presenter.MainPresenter;
import ru.geekbrains.android3_1.mvp.view.MainView;

public class MainActivity extends MvpAppCompatActivity
        implements MainView {

    Button buttonOne;
    Button buttonTwo;
    Button buttonThree;

    @InjectPresenter MainPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonOne = findViewById(R.id.btnCounter1);
        buttonTwo = findViewById(R.id.btnCounter2);
        buttonThree = findViewById(R.id.btnCounter3);

        buttonOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onClickButtonOne();
            }
        });

        buttonTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onClickButtonTwo();
            }
        });

        buttonThree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onClickButtonThree();
            }
        });
    }

    @ProvidePresenter
    public MainPresenter provideMainPresenter(){
        return new MainPresenter();
    }

    @Override
    public void setButtonOneValue(int value) {
        buttonOne.setText(String.format(getString(R.string.countEquals), value));
    }

    @Override
    public void setButtonTwoValue(int value) {
        buttonTwo.setText(String.format(getString(R.string.countEquals), value));
    }

    @Override
    public void setButtonThreeValue(int value) {
        buttonThree.setText(String.format(getString(R.string.countEquals), value));
    }
}
