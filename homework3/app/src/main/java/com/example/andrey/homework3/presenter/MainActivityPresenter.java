package com.example.andrey.homework3.presenter;

import android.annotation.SuppressLint;
import android.support.annotation.DrawableRes;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.example.andrey.homework3.model.ImageConverter;
import com.example.andrey.homework3.view.MainActivityView;

import io.reactivex.Completable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

@InjectViewState
public class MainActivityPresenter extends MvpPresenter<MainActivityView> {

    private static final String outputFileName = "newfile.png";
    private Disposable mDisposable;

    public void onClickConvertButton() {
        getViewState().checkPermission();
    }

    public void doConvert(@DrawableRes int drawableId) {
        ImageConverter converter = new ImageConverter(drawableId, outputFileName);
        mDisposable = converter.getConverterCompletable()
                .doOnSubscribe(disposable -> getViewState().showConvertDialog())
                .subscribe(() -> {
                            getViewState().hideConvertDialog();
                            getViewState().showConvertSuccessMessage();
                        },
                        throwable -> {
                            getViewState().hideConvertDialog();
                            getViewState().showConvertErrorMessage();
                            throwable.printStackTrace();
                });
    }

    public void cancelConvert() {
        if (mDisposable != null && !mDisposable.isDisposed()) {
            mDisposable.dispose();
            getViewState().hideConvertDialog();
            getViewState().showConvertCancelMessage();
        }
    }

}
