package com.example.andrey.homework3.view;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

@StateStrategyType(AddToEndSingleStrategy.class)
public interface MainActivityView extends MvpView {
    @StateStrategyType(SkipStrategy.class)
    void checkPermission();
    @StateStrategyType(SkipStrategy.class)
    void showConvertSuccessMessage();
    @StateStrategyType(SkipStrategy.class)
    void showConvertErrorMessage();
    @StateStrategyType(SkipStrategy.class)
    void showConvertCancelMessage();
    void showConvertDialog();
    void hideConvertDialog();
}
