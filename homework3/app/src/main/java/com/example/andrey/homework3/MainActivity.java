package com.example.andrey.homework3;

import android.Manifest;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.example.andrey.homework3.presenter.MainActivityPresenter;
import com.example.andrey.homework3.view.MainActivityView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends MvpAppCompatActivity
                implements MainActivityView {

    public static final int WRITE_EXTERNAL_STORAGE_REQUEST = 123;

    @InjectPresenter
    MainActivityPresenter mPresenter;
    AlertDialog mDialog;

    @BindView(R.id.convertButton)
    Button mConvertButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.convertButton)
    public void onClickConvertButton() {
        mPresenter.onClickConvertButton();
    }

    @Override
    public void checkPermission() {
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(App.getInstance(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            requestPermissions();
        } else {
            mPresenter.doConvert(R.drawable.car3);
        }
    }

    private void requestPermissions() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                WRITE_EXTERNAL_STORAGE_REQUEST);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case WRITE_EXTERNAL_STORAGE_REQUEST: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mPresenter.doConvert(R.drawable.car3);
                }
            }
            default:
                return;
        }
    }

    @Override
    public void showConvertSuccessMessage() {
        Snackbar.make(findViewById(android.R.id.content), "Convert success!", Snackbar.LENGTH_INDEFINITE).show();
    }

    @Override
    public void showConvertErrorMessage() {
        Snackbar.make(findViewById(android.R.id.content), "Convert fail!", Snackbar.LENGTH_INDEFINITE).show();
    }

    @Override
    public void showConvertCancelMessage() {
        Snackbar.make(findViewById(android.R.id.content), "Convert canceled!", Snackbar.LENGTH_INDEFINITE).show();
    }

    @Override
    public void showConvertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.convert_process)
                .setCancelable(false)
                .setNegativeButton(R.string.cancel_convert, (dialogInterface, i) -> mPresenter.cancelConvert());
        mDialog = builder.create();
        mDialog.show();
    }

    @Override
    public void hideConvertDialog() {
        mDialog.hide();
    }
}
