package com.example.andrey.homework3.model;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;

import com.example.andrey.homework3.App;
import com.example.andrey.homework3.R;

import java.io.File;
import java.io.FileOutputStream;

import io.reactivex.Completable;
import io.reactivex.CompletableEmitter;
import io.reactivex.CompletableOnSubscribe;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Action;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class ImageConverter {

    private String outputName;
    private int fileToConvert;

    public ImageConverter(@DrawableRes int fileToConvert, @NonNull String outputName) {
        this.outputName = outputName;
        this.fileToConvert = fileToConvert;
    }

    public Completable getConverterCompletable() {
        return Completable.fromAction(() -> {
            try {
                Bitmap bitmap = BitmapFactory.decodeResource(App.getInstance().getResources(), fileToConvert);
                File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), outputName);
                try {
                    FileOutputStream fileOutputStream = new FileOutputStream(file);
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream);
                    fileOutputStream.close();
                } catch (Exception e) {
                    throw e;
                }
                Thread.sleep(5000);
            } catch (Exception e) {
                throw e;
            }
        })
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread());
    }
}
