package ru.geekbrains.android3_6;

import com.google.gson.Gson;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Single;
import io.reactivex.observers.TestObserver;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import ru.geekbrains.android3_6.di.DaggerTestComponent;
import ru.geekbrains.android3_6.di.TestComponent;
import ru.geekbrains.android3_6.di.modules.ApiModule;
import ru.geekbrains.android3_6.mvp.model.cache.ICache;
import ru.geekbrains.android3_6.mvp.model.cache.RoomCache;
import ru.geekbrains.android3_6.mvp.model.entity.Repository;
import ru.geekbrains.android3_6.mvp.model.entity.User;
import ru.geekbrains.android3_6.mvp.model.repo.UsersRepo;

import static junit.framework.TestCase.assertEquals;

public class CacheInstrumentedTest {

    @Inject
    @Named("room")
    ICache roomCache;

    @BeforeClass
    public static void setupClass() throws IOException {

    }

    @AfterClass
    public static void tearDownClass() throws IOException {

    }

    @Before
    public void setup(){
        TestComponent component = DaggerTestComponent.create();
        component.inject(this);
    }

    @Test
    public void userTest(){
        User userTo = getTestUser();
        roomCache.putUser(userTo);

        TestObserver<User> observer = new TestObserver<>();
        roomCache.getUser("someuser").subscribe(observer);

        observer.awaitTerminalEvent();
        observer.assertValueCount(1);
        assertEquals(observer.values().get(0).getLogin(), "someuser");
        assertEquals(observer.values().get(0).getAvatarUrl(), "avatar_url");
        assertEquals(observer.values().get(0).getReposUrl(), "repos_url");
    }


    @Test
    public void reposTest(){
        roomCache.putUserRepos(getTestUser(), getTestRepos());

        TestObserver<List<Repository>> observer = new TestObserver<>();
        roomCache.getUserRepos(getTestUser()).subscribe(observer);

        observer.awaitTerminalEvent();
        observer.assertValueCount(1);
        assertEquals(observer.values().get(0).get(0).getId(), "5");
        assertEquals(observer.values().get(0).get(0).getName(), "Repo 5");
        assertEquals(observer.values().get(0).get(1).getId(), "7");
        assertEquals(observer.values().get(0).get(1).getName(), "Repo 7");
    }

    private User getTestUser() {
        return new User("someuser", "avatar_url", "repos_url");
    }

    private ArrayList<Repository> getTestRepos() {
        ArrayList<Repository> repos = new ArrayList<>();
        repos.add(new Repository("5", "Repo 5"));
        repos.add(new Repository("7", "Repo 7"));
        return repos;
    }

}
