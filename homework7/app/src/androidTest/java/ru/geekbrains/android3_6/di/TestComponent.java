package ru.geekbrains.android3_6.di;

import dagger.Component;
import ru.geekbrains.android3_6.CacheInstrumentedTest;
import ru.geekbrains.android3_6.UserRepoInstrumentedTest;
import ru.geekbrains.android3_6.di.modules.CacheModule;
import ru.geekbrains.android3_6.di.modules.RepoModule;

import javax.inject.Singleton;

@Singleton
@Component(modules = {RepoModule.class, CacheModule.class})
public interface TestComponent {
    void inject(UserRepoInstrumentedTest test);
    void inject(CacheInstrumentedTest test);
}
