package ru.geekbrains.android3_5.mvp.model.cache;

import android.graphics.Bitmap;
import android.os.Environment;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import ru.geekbrains.android3_5.mvp.model.entity.Repository;
import ru.geekbrains.android3_5.mvp.model.entity.User;
import ru.geekbrains.android3_5.mvp.model.entity.room.RoomImage;
import ru.geekbrains.android3_5.mvp.model.entity.room.RoomRepository;
import ru.geekbrains.android3_5.mvp.model.entity.room.RoomUser;
import ru.geekbrains.android3_5.mvp.model.entity.room.db.UserDatabase;

public class RoomCache implements IUserCache, IImageCache {

    @Override
    public void saveUser(User user) {
        RoomUser roomUser = UserDatabase.getInstance().getUserDao()
                .findByLogin(user.getLogin());

        if (roomUser == null) {
            roomUser = new RoomUser();
        }

        roomUser.setLogin(user.getLogin());
        roomUser.setAvatarUrl(user.getAvatarUrl());
        roomUser.setReposUrl(user.getReposUrl());

        UserDatabase.getInstance().getUserDao()
                .insert(roomUser);
    }

    @Override
    public User getUser(String username) {
        RoomUser roomUser = UserDatabase.getInstance().getUserDao()
                .findByLogin(username);
        if (roomUser == null) {
            return null;
        }
        return new User(roomUser.getLogin(), roomUser.getAvatarUrl(), roomUser.getReposUrl());
    }

    @Override
    public void saveRepos(String username, List<Repository> repos) {
        if (!repos.isEmpty()) {
            List<RoomRepository> roomRepositories = new ArrayList<>();
            for (Repository repository : repos) {
                RoomRepository roomRepository = new RoomRepository(repository.getId(), repository.getName(), username);
                roomRepositories.add(roomRepository);
            }

            UserDatabase.getInstance()
                    .getRepositoryDao()
                    .insert(roomRepositories);
        }
    }

    @Override
    public List<Repository> getRepos(String username) {
        List<RoomRepository> roomRepositories = UserDatabase.getInstance().getRepositoryDao()
                .getAll();

        List<Repository> repos = new ArrayList<>();
        for (RoomRepository roomRepository: roomRepositories){
            repos.add(new Repository(roomRepository.getId(), roomRepository.getName()));
        }
        return repos;
    }

    @Override
    public void saveImage(String key, Bitmap bitmap) throws Exception {
        String dirPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/MyImageCache/";
        File dir = new File(dirPath);
        if(!dir.exists()) {
            dir.mkdirs();
        }
        String fileName = key + ".png";
        File file = new File(dir, fileName);
        FileOutputStream outputStream = new FileOutputStream(file);
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
        outputStream.flush();
        outputStream.close();

        RoomImage roomImage = UserDatabase.getInstance().getImageDao().findById(key);
        if (roomImage == null) {
            roomImage = new RoomImage();
            roomImage.setId(key);
            roomImage.setPath(dirPath + fileName);
        }
        UserDatabase.getInstance().getImageDao().insert(roomImage);
    }

    @Override
    public String getImage(String key) {
        return UserDatabase.getInstance().getImageDao().findById(key).getPath();
    }
}
