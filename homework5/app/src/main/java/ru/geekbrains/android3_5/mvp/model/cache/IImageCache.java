package ru.geekbrains.android3_5.mvp.model.cache;

import android.graphics.Bitmap;

import ru.geekbrains.android3_5.mvp.model.entity.room.RoomImage;

public interface IImageCache {
    void saveImage(String key, Bitmap bitmap) throws Exception;
    String getImage(String key);
}
