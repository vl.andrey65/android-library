package ru.geekbrains.android3_5.mvp.model.cache;

import java.util.List;

import ru.geekbrains.android3_5.mvp.model.entity.Repository;
import ru.geekbrains.android3_5.mvp.model.entity.User;

public interface IUserCache {
    // user
    void saveUser(User user);
    User getUser(String username);

    // repos
    void saveRepos(String username, List<Repository> repos);
    List<Repository> getRepos(String username);
}
