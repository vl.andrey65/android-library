package ru.geekbrains.android3_5.mvp.model.cache;

import java.util.List;

import io.paperdb.Paper;
import ru.geekbrains.android3_5.mvp.model.entity.Repository;
import ru.geekbrains.android3_5.mvp.model.entity.User;

public class PaperCache implements IUserCache {

    @Override
    public void saveUser(User user) {
        Paper.book("users").write(user.getLogin(), user);
    }

    @Override
    public User getUser(String username) {
        return Paper.book("users").read(username);
    }

    @Override
    public void saveRepos(String username, List<Repository> repos) {
        Paper.book("repos").write(username, repos);
    }

    @Override
    public List<Repository> getRepos(String username) {
        return Paper.book("repos").read(username);
    }
}
