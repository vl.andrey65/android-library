package ru.geekbrains.android3_5.mvp.model.repo;

import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import ru.geekbrains.android3_5.mvp.model.api.ApiHolder;
import ru.geekbrains.android3_5.mvp.model.cache.IUserCache;
import ru.geekbrains.android3_5.mvp.model.entity.Repository;
import ru.geekbrains.android3_5.mvp.model.entity.User;
import ru.geekbrains.android3_5.ui.NetworkStatus;

import java.util.List;

public class UserRepo {

    private IUserCache mCache;

    public UserRepo(IUserCache cache) {
        mCache = cache;
    }

    public Single<User> getUser(String username) {
        if (NetworkStatus.isOnline()) {
            return ApiHolder.getApi().getUser(username)
                    .subscribeOn(Schedulers.io())
                    .map(user -> {
                        mCache.saveUser(user);
                        return user;
                    });
        } else {
            return Single.create(emitter -> {
                User user = mCache.getUser(username);

                if (user == null) {
                    emitter.onError(new RuntimeException("No such user in cache"));
                } else {
                    emitter.onSuccess(user);
                }
            }).subscribeOn(Schedulers.io()).cast(User.class);
        }
    }

    public Single<List<Repository>> getUserRepos(User user) {
        if (NetworkStatus.isOnline()) {
            return ApiHolder.getApi().getUserRepos(user.getReposUrl())
                    .subscribeOn(Schedulers.io())
                    .map(repos -> {

                        mCache.saveUser(user);
                        mCache.saveRepos(user.getLogin(), repos);

                        return repos;
                    });
        } else {

          return Single.create(emitter -> {
                User innerUser = mCache.getUser(user.getLogin());

                if(innerUser == null){
                    emitter.onError(new RuntimeException("No such user in cache"));
                } else {
                    List<Repository> repos = mCache.getRepos(user.getLogin());
                    emitter.onSuccess(repos);
                }
            }).subscribeOn(Schedulers.io()).cast((Class<List<Repository>>)(Class)List.class);
        }
    }
}
