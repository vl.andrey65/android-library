package ru.geekbrains.android3_5.mvp.model.cache;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import ru.geekbrains.android3_5.mvp.model.entity.Repository;
import ru.geekbrains.android3_5.mvp.model.entity.User;
import ru.geekbrains.android3_5.mvp.model.entity.realm.RealmRepository;
import ru.geekbrains.android3_5.mvp.model.entity.realm.RealmUser;

public class RealmCache implements IUserCache {

    @Override
    public void saveUser(User user) {
        Realm realm = Realm.getDefaultInstance();
        RealmUser realmUser = realm.where(RealmUser.class).equalTo("login", user.getLogin()).findFirst();
        if (realmUser == null) {
            realm.executeTransaction(innerRealm -> {
                RealmUser newRealmUser = innerRealm.createObject(RealmUser.class, user.getLogin());
                newRealmUser.setAvatarUrl(user.getAvatarUrl());
                newRealmUser.setReposUrl(user.getReposUrl());
            });
        } else {
            realm.executeTransaction(innerRealm -> {
                realmUser.setAvatarUrl(user.getAvatarUrl());
                realmUser.setReposUrl(user.getReposUrl());
            });
        }
        realm.close();
    }

    @Override
    public User getUser(String username) {
        Realm realm = Realm.getDefaultInstance();
        RealmUser realmUser = realm.where(RealmUser.class).equalTo("login", username).findFirst();
        if (realmUser == null) {
            return null;
        }
        User user =  new User(realmUser.getLogin(), realmUser.getAvatarUrl(), realmUser.getReposUrl());
        realm.close();
        return user;
    }

    @Override
    public void saveRepos(String username, List<Repository> repos) {
        Realm realm = Realm.getDefaultInstance();
        RealmUser realmUser = realm.where(RealmUser.class).equalTo("login", username).findFirst();

        if (realmUser != null) {
            realm.executeTransaction(innerRealm -> {
                realmUser.getRepos().deleteAllFromRealm();
                for (Repository repository : repos) {
                    RealmRepository realmRepository = innerRealm.createObject(RealmRepository.class, repository.getId());
                    realmRepository.setName(repository.getName());
                    realmUser.getRepos().add(realmRepository);
                }
            });
        }
        realm.close();
    }

    @Override
    public List<Repository> getRepos(String username) {
        Realm realm = Realm.getDefaultInstance();
        RealmUser realmUser = realm.where(RealmUser.class).equalTo("login", username).findFirst();
        List<Repository> repos = new ArrayList<>();
        for (RealmRepository realmRepository : realmUser.getRepos()) {
            repos.add(new Repository(realmRepository.getId(), realmRepository.getName()));
        }
        return repos;
    }
}
