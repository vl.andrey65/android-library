package ru.geekbrains.hw3.mvp.view;

public interface MainView {
    void pickImage();

    void showConvertProgressDialog();

    void dismissConvertProgressDialog();

    void showConvertationSuccessMessage();

    void showConvertationCanceledMessage();

    void showConvertationFailedMessage();
}
