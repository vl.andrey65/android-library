package ru.geekbrains.hw3.mvp.model;

import io.reactivex.Completable;

public interface ImageConverter
{
    Completable convertJpegToPng(String source, String dest);
}
