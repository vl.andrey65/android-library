package ru.geekbrains.android3_4.mvp.presenter;

import java.util.ArrayList;

import ru.geekbrains.android3_4.mvp.model.entity.Repository;
import ru.geekbrains.android3_4.mvp.view.ReposHolderView;

public interface IReposPresenter {
    void bindView(ReposHolderView holder, int pos);
    int getItemsCount();
    void setItems(ArrayList<Repository> repos);
}
