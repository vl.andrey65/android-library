package ru.geekbrains.android3_4.mvp.view;

public interface ReposHolderView {
    void setRepoName(String repoName);
}
