package ru.geekbrains.android3_4.ui.activity;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;

import java.util.ArrayList;

import io.reactivex.android.schedulers.AndroidSchedulers;
import ru.geekbrains.android3_4.R;
import ru.geekbrains.android3_4.mvp.model.entity.Repository;
import ru.geekbrains.android3_4.mvp.model.image.IImageLoader;
import ru.geekbrains.android3_4.mvp.presenter.MainPresenter;
import ru.geekbrains.android3_4.mvp.view.MainView;
import ru.geekbrains.android3_4.ui.ReposAdapter;
import ru.geekbrains.android3_4.ui.image.GlideImageLoader;

public class MainActivity extends MvpAppCompatActivity implements MainView {
    @InjectPresenter
    MainPresenter presenter;

    @BindView(R.id.tv_username)
    TextView usernameTextView;
    @BindView(R.id.iv_avatar)
    ImageView avatarImageView;
    @BindView(R.id.recycler)
    RecyclerView recyclerView;

    LinearLayoutManager layoutManager;
    ReposAdapter adapter;
    IImageLoader<ImageView> imageLoader = new GlideImageLoader();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        initRecycler();
    }

    private void initRecycler() {
        layoutManager = new LinearLayoutManager(this);
        adapter = new ReposAdapter(presenter.getReposPresenter());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    @ProvidePresenter
    public MainPresenter provideMainPresenter(){
        return new MainPresenter(AndroidSchedulers.mainThread());
    }

    @Override
    public void showMessage(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setImageUrl(String url) {
        imageLoader.loadInto(url,avatarImageView);
    }

    @Override
    public void setUsernametext(String text) {
        usernameTextView.setText(text);
    }

    @Override
    public void updateRepos() {
        adapter.notifyDataSetChanged();
    }
}
