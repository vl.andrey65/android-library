package ru.geekbrains.android3_4.ui;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.geekbrains.android3_4.R;
import ru.geekbrains.android3_4.mvp.model.entity.Repository;
import ru.geekbrains.android3_4.mvp.presenter.IReposPresenter;
import ru.geekbrains.android3_4.mvp.view.ReposHolderView;

public class ReposAdapter extends RecyclerView.Adapter<ReposAdapter.ReposHolder> {

    IReposPresenter mPresenter;

    public ReposAdapter(IReposPresenter presenter) {
        mPresenter = presenter;
    }

    @NonNull
    @Override
    public ReposHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item_repo, viewGroup, false);
        ReposHolder holder = new ReposHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ReposHolder holder, int i) {
        mPresenter.bindView(holder, i);
    }

    @Override
    public int getItemCount() {
        return mPresenter.getItemsCount();
    }

    public class ReposHolder extends RecyclerView.ViewHolder
            implements ReposHolderView {

        @BindView(R.id.repo_name)
        TextView mRepoName;

        public ReposHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void setRepoName(String repoName) {
            mRepoName.setText(repoName);
        }
    }
}
