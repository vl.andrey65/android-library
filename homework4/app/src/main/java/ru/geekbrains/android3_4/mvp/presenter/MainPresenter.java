package ru.geekbrains.android3_4.mvp.presenter;

import android.annotation.SuppressLint;
import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import java.util.ArrayList;

import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import ru.geekbrains.android3_4.mvp.model.api.ApiHolder;
import ru.geekbrains.android3_4.mvp.model.entity.Repository;
import ru.geekbrains.android3_4.mvp.model.repo.UsersRepo;
import ru.geekbrains.android3_4.mvp.view.MainView;
import ru.geekbrains.android3_4.mvp.view.ReposHolderView;
import timber.log.Timber;

@InjectViewState
public class MainPresenter extends MvpPresenter<MainView> {

    public class ReposPresenter implements IReposPresenter {

        private ArrayList<Repository> mItems = new ArrayList<>();

        @Override
        public void setItems(ArrayList<Repository> repos) {
            mItems = repos;
        }

        @Override
        public void bindView(ReposHolderView holder, int pos) {
            Repository repo = mItems.get(pos);
            holder.setRepoName(repo.getName());
        }

        @Override
        public int getItemsCount() {
            return mItems.size();
        }
    }

    Scheduler mainThreadScheduler;
    UsersRepo usersRepo;
    ReposPresenter mReposPresenter;

    public MainPresenter(Scheduler mainThreadScheduler) {
        this.mainThreadScheduler = mainThreadScheduler;
        this.usersRepo = new UsersRepo();
        this.mReposPresenter = new ReposPresenter();
    }

    @SuppressLint("CheckResult")
    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        loadUser();
    }

    @SuppressLint("CheckResult")
    private void loadUser() {
        usersRepo.getUser("googlesamples")
                .observeOn(mainThreadScheduler)
                .subscribe(user -> {
                            getViewState().setUsernametext(user.getLogin());
                            getViewState().setImageUrl(user.getAvatarUrl());
                            loadRepos(user.getReposUrl());
                        },
                        throwable -> {
                            Timber.e(throwable);
                        });
    }

    @SuppressLint("CheckResult")
    private void loadRepos(String url) {
        usersRepo.getRepos(url)
                .observeOn(mainThreadScheduler)
                .subscribe(repositories -> {
                    mReposPresenter.setItems(repositories);
                    getViewState().updateRepos();
                }, throwable -> {
                    getViewState().showMessage("Ошибка!");
                });
    }


    @SuppressLint("CheckResult")
    private void loadDataWithOkHttp() {
        Single<String> single = Single.fromCallable(() -> {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url("https://api.github.com/users/googlesamples")
                    .build();
            return client.newCall(request).execute().body().string();
        });

        single.subscribeOn(Schedulers.io())
                .subscribe(string -> Timber.d(string));
    }

    public ReposPresenter getReposPresenter() {
        return mReposPresenter;
    }
}
