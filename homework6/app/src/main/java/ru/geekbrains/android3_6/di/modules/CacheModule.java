package ru.geekbrains.android3_6.di.modules;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import ru.geekbrains.android3_6.mvp.model.cache.ICache;
import ru.geekbrains.android3_6.mvp.model.cache.IImageCache;
import ru.geekbrains.android3_6.mvp.model.cache.RealmCache;
import ru.geekbrains.android3_6.mvp.model.cache.RoomCache;

@Module
public class CacheModule {

    @Provides
    @Named("RoomCache")
    public ICache roomCache(){
        return new RoomCache();
    }

    @Provides
    @Named("RealmCache")
    public ICache realmCache(){
        return new RealmCache();
    }

    @Provides
    @Named("RoomImageCache")
    public IImageCache roomImageCache() { return new RoomCache(); }

    @Provides
    @Named("RealmImageCache")
    public IImageCache realmImageCache() { return new RealmCache(); }
}
