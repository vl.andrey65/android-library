package ru.geekbrains.android3_6.di.modules;

import android.widget.ImageView;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import ru.geekbrains.android3_6.mvp.model.cache.IImageCache;
import ru.geekbrains.android3_6.mvp.model.image.ImageLoader;
import ru.geekbrains.android3_6.mvp.model.image.android.ImageLoaderGlide;

@Module
public class ImageModule {

    @Provides
    public ImageLoader<ImageView> imageLoader(@Named("RoomImageCache") IImageCache cache) {
        return new ImageLoaderGlide(cache);
    }
}
