package ru.geekbrains.android3_6.mvp.model.cache;

import android.graphics.Bitmap;
import android.os.Environment;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.SingleEmitter;
import io.reactivex.SingleOnSubscribe;
import io.reactivex.schedulers.Schedulers;
import io.realm.Realm;
import ru.geekbrains.android3_6.mvp.model.entity.Repository;
import ru.geekbrains.android3_6.mvp.model.entity.User;
import ru.geekbrains.android3_6.mvp.model.entity.realm.CachedImage;
import ru.geekbrains.android3_6.mvp.model.entity.realm.RealmUserRepository;
import ru.geekbrains.android3_6.mvp.model.entity.realm.RealmUser;

public class RealmCache implements ICache, IImageCache {

    @Override
    public void putUser(User user) {
        Realm realm = Realm.getDefaultInstance();
        RealmUser realmUser = realm.where(RealmUser.class).equalTo("login", user.getLogin()).findFirst();
        if (realmUser == null) {
            realm.executeTransaction(innerRealm -> {
                RealmUser newRealmUser = innerRealm.createObject(RealmUser.class, user.getLogin());
                newRealmUser.setAvatarUrl(user.getAvatarUrl());
            });
        } else {
            realm.executeTransaction(innerRealm -> {
                realmUser.setAvatarUrl(user.getAvatarUrl());
            });
        }
        realm.close();
    }

    @Override
    public Single<User> getUser(String username) {
        return Single.create((SingleOnSubscribe<User>) emitter -> {
            Realm realm = Realm.getDefaultInstance();
            RealmUser realmUser = realm.where(RealmUser.class).equalTo("login", username).findFirst();
            if (realmUser == null) {
                emitter.onError(new RuntimeException());
            } else {
                User user =  new User(realmUser.getLogin(), realmUser.getAvatarUrl(), null);
                emitter.onSuccess(user);
            }
            realm.close();
        }).subscribeOn(Schedulers.io()).cast(User.class);
    }

    @Override
    public void putUserRepos(User user, List<Repository> repos) {
        Realm realm = Realm.getDefaultInstance();
        RealmUser realmUser = realm.where(RealmUser.class).equalTo("login", user.getLogin()).findFirst();

        if (realmUser != null) {
            realm.executeTransaction(innerRealm -> {
                realmUser.getRepositories().deleteAllFromRealm();
                for (Repository repository : repos) {
                    RealmUserRepository realmRepository = innerRealm.createObject(RealmUserRepository.class, repository.getId());
                    realmRepository.setName(repository.getName());
                    realmUser.getRepositories().add(realmRepository);
                }
            });
        }
        realm.close();
    }

    @Override
    public Single<List<Repository>> getUserRepos(User user) {
        return Single.create((SingleOnSubscribe<List<Repository>>) emitter -> {
            Realm realm = Realm.getDefaultInstance();
            RealmUser realmUser = realm.where(RealmUser.class).equalTo("login", user.getLogin()).findFirst();

            if (realmUser == null) {
                emitter.onError(new RuntimeException());
            } else {
                List<Repository> repos = new ArrayList<>();
                for (RealmUserRepository realmRepository : realmUser.getRepositories()) {
                    repos.add(new Repository(realmRepository.getId(), realmRepository.getName()));
                }
                emitter.onSuccess(repos);
            }
            realm.close();
        }).subscribeOn(Schedulers.io()).cast((Class<List<Repository>>)(Class)List.class);
    }

    @Override
    public void saveImage(String key, Bitmap bitmap) throws Exception {
        String dirPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/MyImageCache/";
        File dir = new File(dirPath);
        if(!dir.exists()) {
            dir.mkdirs();
        }
        String fileName = key + ".png";
        File file = new File(dir, fileName);
        FileOutputStream outputStream = new FileOutputStream(file);
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
        outputStream.flush();
        outputStream.close();

        Realm.getDefaultInstance().executeTransactionAsync(realm -> {
            CachedImage cachedImage = new CachedImage();
            cachedImage.setUrl(key);
            cachedImage.setPath(dirPath + fileName);
            realm.copyToRealm(cachedImage);
        });

        return;
    }

    @Override
    public String getImage(String key) {
        CachedImage cachedImage = Realm.getDefaultInstance().where(CachedImage.class).equalTo("url", key).findFirst();
        if (cachedImage != null) {
            return cachedImage.getPath();
        }
        return null;
    }
}
