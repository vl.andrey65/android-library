package ru.geekbrains.android3_6.mvp.model.image.android;

import android.graphics.Bitmap;
import android.support.annotation.Nullable;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import ru.geekbrains.android3_6.NetworkStatus;
import ru.geekbrains.android3_6.mvp.common.Utils;
import ru.geekbrains.android3_6.mvp.model.cache.IImageCache;
import ru.geekbrains.android3_6.mvp.model.image.ImageLoader;
import timber.log.Timber;

public class ImageLoaderGlide implements ImageLoader<ImageView> {

    private IImageCache mCache;

    public ImageLoaderGlide(IImageCache cache) {
        mCache = cache;
    }

    @Override
    public void loadInto(@Nullable String url, ImageView container) {
        if (NetworkStatus.isOnline()) {
            GlideApp.with(container.getContext()).asBitmap().load(url).listener(new RequestListener<Bitmap>() {
                @Override
                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Bitmap> target, boolean isFirstResource) {
                    Timber.e(e, "Image load failed");
                    return false;
                }

                @Override
                public boolean onResourceReady(Bitmap resource, Object model, Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {
                    try {
                        mCache.saveImage(Utils.SHA1(url), resource);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return false;
                }
            }).into(container);
        } else {
            Glide.with(container.getContext())
                    .load(mCache.getImage(Utils.SHA1(url)))
                    .into(container);
        }
    }

}
