package ru.geekbrains.android3_6.mvp.model.entity.room.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import ru.geekbrains.android3_6.mvp.model.entity.room.RoomImage;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

@Dao
public interface ImageDao {

    @Insert(onConflict = REPLACE)
    void insert(RoomImage roomImage);

    @Insert(onConflict = REPLACE)
    void insert(RoomImage... roomImages);

    @Insert(onConflict = REPLACE)
    void insert(List<RoomImage> roomImages);


    @Update
    void update(RoomImage roomImage);

    @Update
    void update(RoomImage... roomImages);

    @Update
    void update(List<RoomImage> roomImages);


    @Delete
    void delete(RoomImage roomImage);

    @Delete
    void delete(RoomImage... roomImages);

    @Delete
    void delete(List<RoomImage> roomImages);

    @Query("SELECT * FROM roomimage")
    List<RoomImage> getAll();

    @Query("SELECT * FROM roomimage WHERE id = :id LIMIT 1")
    RoomImage findById(String id);

}
