package ru.geekbrains.android3_6.mvp.model.cache;

import java.util.List;

import io.paperdb.Paper;
import io.reactivex.Single;
import io.reactivex.SingleEmitter;
import io.reactivex.SingleOnSubscribe;
import io.reactivex.schedulers.Schedulers;
import ru.geekbrains.android3_6.mvp.model.entity.Repository;
import ru.geekbrains.android3_6.mvp.model.entity.User;

public class PaperCache implements ICache {

    @Override
    public void putUser(User user) {
        Paper.book("users").write(user.getLogin(), user);
    }

    @Override
    public Single<User> getUser(String username) {
        return Single.create((SingleOnSubscribe<User>) emitter -> {
            User user = Paper.book("users").read(username);
            if (user == null) {
                emitter.onError(new RuntimeException("No such user in cache"));
            } else {
                emitter.onSuccess(user);
            }
        }).subscribeOn(Schedulers.io()).cast(User.class);
    }

    @Override
    public void putUserRepos(User user, List<Repository> repos) {
        Paper.book("repos").write(user.getLogin(), repos);
    }

    @Override
    public Single<List<Repository>> getUserRepos(User user) {
        return Single.create(emitter -> {
            List<Repository> list = Paper.book("repos").read(user.getLogin());
            if (list == null) {
                emitter.onError(new RuntimeException());
            } else {
                emitter.onSuccess(list);
            }
        }).subscribeOn(Schedulers.io()).cast((Class<List<Repository>>)(Class)List.class);
    }
}
